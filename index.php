<?php
require_once 'start.php';
require 'jsmin.php';
?>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <title>Brother of the Sun</title>
    <meta name="p:domain_verify" content="dad07b4f727422d6da056047051090e2"/>
    <meta name="author" content="Muhamad Fajar" />
    <meta name="Description" content="Personal web dari Muhamad Fajar."/>
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-66336742-1', 'auto');
      ga('send', 'pageview');
    </script>
    <link rel="stylesheet" href="compress.php" type="text/css" media="screen" />
  </head>
<body ng-app="ng-terminal" oncontextmenu="return false">
  <div ng-controller="console"><terminal></terminal></div>
  <?php
  if(!file_exists('angular/angular.min.js')){
    $output[0] = JSMin::minify(file_get_contents('js/angular.js'));
    file_put_contents('angular/angular.min.js', $output[0]);
    $output[1] = JSMin::minify(file_get_contents('js/core.js'));
    file_put_contents('angular/core.min.js', $output[1]);
    $output[2] = JSMin::minify(file_get_contents('js/tools.js'));
    file_put_contents('angular/tools.min.js', $output[2]);
    $output[3] = JSMin::minify(file_get_contents('js/terminal.js'));
    file_put_contents('angular/terminal.min.js', $output[3]);
    $output[4] = JSMin::minify(file_get_contents('js/implementations.js'));
    file_put_contents('angular/implementations.min.js', $output[4]);
    $output[5] = JSMin::minify(file_get_contents('js/filesystem.js'));
    file_put_contents('angular/filesystem.min.js', $output[5]);
    $output[6] = JSMin::minify(file_get_contents('js/bots.js'));
    file_put_contents('angular/bots.min.js', $output[6]);
  }
  ?>
  <script src="angular/angular.min.js"></script>
  <script src="angular/core.min.js"></script>
  <script src="angular/tools.min.js"></script>
  <script src="angular/terminal.min.js"></script>
  <script src="angular/implementations.min.js"></script>
  <script src="angular/filesystem.min.js"></script>
  <script src="angular/bots.min.js"></script>
</body>
</html>
<?php
require_once 'end.php';
?>
