<?php
header('Content-type: text/css');
ob_start('compress_css');
function compress_css($buffer) {
  $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
  $buffer = str_replace(array("\r", "\n", "\r\n", "\t", '  ', '    ', '    '), '', $buffer);
  return $buffer;
}
require_once 'css/main.min.css';
ob_end_flush();
?>
